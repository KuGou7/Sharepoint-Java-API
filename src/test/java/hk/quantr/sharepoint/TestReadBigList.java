/*
 * Here comes the text of your license
 * Each line should be prefixed with  * 
 */
package hk.quantr.sharepoint;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

/**
 *
 * @author peter
 */
public class TestReadBigList {

	@Test
	public void readBigList() throws FileNotFoundException, IOException {

		List<String> lines = IOUtils.readLines(new FileReader(System.getProperty("user.home") + File.separator + "password.txt"));
		String password = lines.get(0);
		String domain = "quantr";
		Pair<String, String> token = SPOnline.login("wordpress@quantr.hk", password, domain);
		if (token != null) {
			JSONObject json = SPOnline.postJSON(token, domain, "/_api/contextinfo", null, null);
			String formDigestValue = json.getJSONObject("d").getJSONObject("GetContextWebInformation").getString("FormDigestValue");
			System.out.println("FormDigestValue=" + formDigestValue);

			json = SPOnline.getJSON(token, domain, "/gitlab/_api/web/lists/GetByTitle('Commit')/itemcount");
			int noOfItem = json.getJSONObject("d").getInt("ItemCount");

			for (int row = 0; row < noOfItem; row += 5000) {
				json = SPOnline.getJSON(token, domain, "/gitlab/_api/web/lists/GetByTitle('Commit')/items?$top=5000&$skip=" + row);
				if (json != null) {
					JSONArray arr = json.getJSONObject("d").getJSONArray("results");
					for (int x = 0; x < arr.length(); x++) {
						String ProjectName = arr.getJSONObject(x).getString("Title");
						System.out.println((x + 1 + row) + " : " + ProjectName);
					}
				}
			}
		}
	}
}
